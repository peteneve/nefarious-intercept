<?php
################################################################################
#
# Intercept and block potentially nefarious visitors from your site.
#
# Currently allows blocking of TOR, scrapers and scanning companies.
# The average user is unlikely to come from any of these.
#
# OPTIONS ######################################################################
$REDIRECT_URL = FALSE; # If false return 404
$BLOCK_TOR    = TRUE;
$BLOCK_BOTS   = TRUE;
$BLOCK_HOSTS  = TRUE;

# Used for CLI debugging
#$_SERVER['HTTP_USER_AGENT']='Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0';
#$_SERVER['REMOTE_ADDR'] = '8.8.8.8';

# Static Variables #############################################################
# User Agent strings for bots
$bots_useragents = array("bot","google","crawler","baidu","bing","msn",
  "duckduckgo","teoma","slurp","yandex","spider","facebookexternalhit",
  "facebotrobot","crawling","pycurl");

# URL for list of TOR ip addresses
$tor_ips_url = 'https://secureupdates.checkpoint.com/IP-list/TOR.txt';

$blocked_hosts = array("google","amazonaws","cyveillance","phishtank",
  "netpilot","calyxinstitute","tor-exit","mimecast","archive");

# FUNCTIONS ####################################################################
function redirect($url)
{
  if ($url===FALSE) {
    header('HTTP/1.0 404 Not Found');
    die("<h1>404 Not Found</h1>The page that you have requested could not be found.\n\n");
  } else {
    header('Location: '.$url);
    exit;
  }
}

function get_tor_ips($tor_ips_url) {
  $tor_store = 'tor_ips.txt';
  if (file_exists($tor_store)) {
    if (time()-filemtime($tor_store) > 2 * 3600) {
      // file older than 2 hours
      file_put_contents ( $tor_store , file($tor_ips_url));
    }
  } else {
    file_put_contents ( $tor_store , file($tor_ips_url));
  }
  return file($tor_store, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
}

# Start blocking ###############################################################
if ($BLOCK_TOR === TRUE) {
  $tor_ips = get_tor_ips($tor_ips_url);
  if (in_array($_SERVER['REMOTE_ADDR'], $tor_ips)) {
    redirect($REDIRECT_URL);
  }
}

if ($BLOCK_BOTS === TRUE) {
  if (preg_match('/' . implode('|', $bots_useragents) . '/i', $_SERVER['HTTP_USER_AGENT'])) {
    redirect($REDIRECT_URL);
  }
}

if ($BLOCK_HOSTS === TRUE) {
  if (preg_match('/' . implode('|', $blocked_hosts) . '/i', gethostbyaddr($_SERVER['REMOTE_ADDR']))) {
    print "hostname:" . gethostbyaddr($_SERVER['REMOTE_ADDR']);
    redirect($REDIRECT_URL);
  }
}

?>
